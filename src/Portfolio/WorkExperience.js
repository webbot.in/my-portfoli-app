import { Avatar, Box, Button, Card, Divider, Stack, Typography } from '@mui/material'
import React from 'react'
import ncpliLoco from './img/ncpliloco.png'
import itechLoco from './img/itechloco.jpeg'
import FileDownloadIcon from '@mui/icons-material/FileDownload';

function WorkExperience() {
  const pastYear = 2022
  const currentYear = new Date()

  const workExperienceDetails = [{
    loco: ncpliLoco,
    role: "Web Developer",
    companyName: "Netcom Computers Private Limited",
    startDate: "Jan 2022",
    endDate: "Present",
    experienceYears: currentYear.getFullYear() - pastYear + " Year",
    experienceMonths: currentYear.getMonth() + 1 + " Months"
  }, {
    loco: itechLoco,
    role: "Junior Executive",
    companyName: "iTech India Private Limited",
    startDate: "Sep 2019",
    endDate: "Feb 2020",
    experienceYears: "0 Year",
    experienceMonths: "6 Months"
  }]

  const titleFontSize = { xs: 22, sm: 30, md: 18, lg: 20, xl: 30 }
  const cardFontSize = { xs: 12, sm: 20, md: 12, lg: 15, xl: 20 }


  return (
    <>
      <Box>
        <Divider />
        <Stack direction={{ md: "row" }} justifyContent={"space-between"} mt={5} >
          <Typography sx={{ color: "#2DCDCE", fontFamily: "Poppins", fontSize: titleFontSize }}>Work Experience</Typography>

          <Stack direction={{ md: "row" }} gap={{ xs: 2, sm: 5, md: 5, lg: 9, xl: 5 }} alignItems={"center"} justifyContent={"flex-end"} sx={{ mt: { xs: 3, md: 0 }, width: { xs: "100%", md: "80%" } }}>
            {workExperienceDetails.map((items, index) => (
              <Card elevation={5} sx={{borderRadius:"10px"}}>
                <Stack gap={2} sx={{ ml: 2, mt: 2, mr: index === 0 ? { xs: 2 } : { xs: 7, sm: 10, md: 7, lg: 8 } }}>
                  <Stack direction={"row"} gap={3} >
                    <Avatar sx={{ width: 56, height: 56, overflow: 'hidden', border: "1px solid #404040" }}> <Box component={"img"} src={items.loco} sx={{ width: "100%", height: "100%", backgroundSize: "100% 100%" }} /> </Avatar>
                    <Stack gap={1}>
                      <Typography sx={{ fontSize: cardFontSize, fontFamily: "Ubuntu" }}>{items.role}</Typography>
                      <Typography sx={{ fontSize: cardFontSize, fontFamily: "fantasy" }}>{items.companyName}</Typography>
                      <Typography sx={{ fontSize: cardFontSize, fontFamily: "fantasy" }}>{items.startDate} - {items.endDate}</Typography>
                    </Stack>
                  </Stack>
                </Stack>

                <Stack alignItems={"center"} sx={{ mt: 2 }}>
                <Divider sx={{width:"93%"}}/>
                </Stack>
                <Typography sx={{ fontSize: cardFontSize, fontFamily: "Poppins", textAlign: "end", mt: 1, mr: 2, mb: 1 }}>{items.experienceYears} {items.experienceMonths}</Typography>

              </Card>
            ))}
          </Stack>
        </Stack>

        <Divider sx={{ mt: 8 }} />

        <Stack direction={{ md: "row" }} justifyContent={"space-between"} alignItems={{ md: "center" }} gap={3} mt={5}>
          <Typography sx={{ color: "#2DCDCE", fontFamily: "Poppins", fontSize: titleFontSize }}>Personal Projects</Typography>
          <Stack direction={{ md: "row" }} alignItems={"center"} gap={5}>
            <Button onClick={() => window.open("https://project.webbot.in/")} variant='contained' sx={{ borderRadius: "50px", bgcolor: "black", ":hover": { bgcolor: "black" } }}>Studio</Button>
            <Button onClick={() => window.open("https://task.webbot.in/")} variant='contained' sx={{ borderRadius: "50px", bgcolor: "black", ":hover": { bgcolor: "black" } }}>CORS Error Fix</Button>
            <Button onClick={() => window.open("https://webtask.webbot.in/")} variant='contained' sx={{ borderRadius: "50px", bgcolor: "black", ":hover": { bgcolor: "black" } }}>Crud Operation</Button>
          </Stack>
        </Stack>

        <Divider sx={{ mt: 8 }} />

        <Stack direction={{ md: "row" }} justifyContent={"space-between"} alignItems={{ md: "center" }} gap={3} mt={5}>
          <Typography sx={{ color: "#2DCDCE", fontFamily: "Poppins", fontSize: titleFontSize }}>Company Projects</Typography>
          <Stack direction={{ md: "row" }} alignItems={"center"} gap={5}>
            <Button onClick={() => window.open("https://ncpli.com/")} variant='contained' sx={{ borderRadius: "50px", bgcolor: "black", ":hover": { bgcolor: "black" } }}>ncpli</Button>
            <Button onClick={() => window.open("https://tailsuite.com/")} variant='contained' sx={{ borderRadius: "50px", bgcolor: "black", ":hover": { bgcolor: "black" } }}>Tailsuite</Button>
            <Button onClick={() => window.open("https://www.shivanedesigning.com/")} variant='contained' sx={{ borderRadius: "50px", bgcolor: "black", ":hover": { bgcolor: "black" } }}>shivanedesigning</Button>
          </Stack>
        </Stack>

        <Stack direction={{ md: "row" }} justifyContent={"space-between"} alignItems={{ md: "center" }} gap={3} mt={8} width={{ md: "92%" }}>
          <Typography sx={{ color: "#2DCDCE", fontFamily: "Poppins", fontSize: titleFontSize }}>Ongoing Project</Typography>
          <Stack direction={{ md: "row" }} alignItems={"center"} gap={5}>
            <Button onClick={() => window.open("https://gillgall.com/")} variant='contained' sx={{ borderRadius: "50px", bgcolor: "black", ":hover": { bgcolor: "black" } }}>gillgall webapplication</Button>
          </Stack>
        </Stack>


        <Divider sx={{ mt: 8 }} />
        <Typography sx={{ color: "#ff4d4d", mt: 5, textAlign: "center", fontSize: { xs: 15, sm: 30, md: 18, lg: 20, xl: 30 } }}>This website is currently being developed...</Typography>
        <Stack alignItems={"center"} mb={8} mt={5} >
          <Button variant='contained' onClick={() => window.open("https://drive.google.com/uc?id=1Lph_lJVqIJbv_DzDSIAajMwtYql6ptZg&export=download")} sx={{ textTransform: "capitalize", borderRadius: "50px", bgcolor: "#2DCDCE", ":hover": { bgcolor: "#2DCDCE" } }}> <FileDownloadIcon sx={{ mr: 1 }} /> My Resume</Button>
        </Stack>
      </Box>
    </>
  )
}

export default WorkExperience