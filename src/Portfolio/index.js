import React, { useEffect, useRef} from 'react'
import Logo from './img/webbot_logo_black.png'
import { Box, Stack } from '@mui/material'
import Intro from './Intro'
import WorkExperience from './WorkExperience'


function Portfolio() {

    const scrollRef = useRef(null);

    useEffect(() => {
        if (scrollRef.current) {
            scrollRef.current.scrollIntoView({ behavior: 'smooth' });
        }
    }, []);

    return (
        <>
            <Stack alignItems={"center"} ref={scrollRef}>
                <Box width={{ xs: "95%", md: "90%", lg: "80%" }} >
                    <Stack alignItems={"center"} direction={"row"} justifyContent={"space-between"} width={"60%"}>
                        <Box component={"img"} src={Logo} draggable={false} alt='logo' mt={{xs:1,md:2}} />
                    </Stack>
                    <Stack gap={10}>
                        <Intro />
                        <WorkExperience />
                    </Stack>
                </Box>
            </Stack>
        </>
    )
}

export default Portfolio