import React, { useEffect } from 'react'
import Portfolio from "./Portfolio";

function App() {
  useEffect(() => {
    function handleContextMenu(event) {
      event.preventDefault();
    }

    document.addEventListener('contextmenu', handleContextMenu);

    return () => {
      document.removeEventListener('contextmenu', handleContextMenu);
    };
  }, []);
  return (
   <>
   <Portfolio />
   </>
  );
}

export default App;
